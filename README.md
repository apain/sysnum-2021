# A net-list simulator: Sysnum project, part 1

## Instructions

The simulator can be compiled using the command `ocamlbuild netlist_simulator.byte`.

The simulator can be run on a test file using the command `./netlist_simulator.byte [filename]`. Several test files are included in the `test` directory.

Optional command-line arguments are to be included before the file name. They include:

- `-n X` where X is an integer: specify the number of steps of the simulation. If not specified, the simulation will run for 100 steps.

- `-print`: the scheduled netlist will not be simulated but merely printed on standard output.

## Files

The files included in this directory are:

- `graph.ml`: functions to detect if a graph has a cycle and to output a topological sort of its nodes

- `netlist_ast.ml`, `netlist_lexer.mll`, `netlist_parser.mly`: files used for the compilation of the netlists

- `scheduler.ml`: sorts the netlist to allow evaluation

- `netlist_simulator.ml`: the simulation of the netlist, evaluating its equations and implementing registers, ROM and RAM

- `netlist_printer.ml`: functions for printing netlist elements

- `tools.ml`: useful functions easing the manipulation of types

- `test` directory: contains a number of files on which the simulator can be tested

## Git repository for the simulator

Link to [Git repository](https://git.eleves.ens.fr/apain/sysnum-2021).
