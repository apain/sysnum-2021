exception Cycle
type mark = NotVisited | InProgress | Visited

type 'a graph =
    { mutable g_nodes : 'a node list }
and 'a node = {
  n_label : 'a;
  mutable n_mark : mark;
  mutable n_link_to : 'a node list;
  mutable n_linked_by : 'a node list;
}

let mk_graph () = { g_nodes = [] }

let add_node g x =
  let n = { n_label = x; n_mark = NotVisited; n_link_to = []; n_linked_by = [] } in
  g.g_nodes <- n :: g.g_nodes

let node_of_label g x =
  List.find (fun n -> n.n_label = x) g.g_nodes

let add_edge g id1 id2 =
  try
    let n1 = node_of_label g id1 in
    let n2 = node_of_label g id2 in
    n1.n_link_to   <- n2 :: n1.n_link_to;
    n2.n_linked_by <- n1 :: n2.n_linked_by
  with Not_found -> Format.eprintf "Tried to add an edge between non-existing nodes"; raise Not_found

let clear_marks g =
  List.iter (fun n -> n.n_mark <- NotVisited) g.g_nodes
 
let rec visit n = 
        if n.n_mark = InProgress then true
        else if n.n_mark = NotVisited then 
                (n.n_mark <- InProgress;
                List.exists visit n.n_link_to || (n.n_mark <- Visited; false))
        else false      

let has_cycle g = 
        clear_marks g;
        match g.g_nodes with
        | [] -> false
        | l -> let rec walk l = match l with
                | [] -> false
                | n::t when n.n_mark = Visited -> walk t
                | n::_ when n.n_mark = InProgress -> true
                | n::t -> visit n || walk t
        in walk l

let topological g =
        clear_marks g;
        let rec dfs n endlist =
                n.n_mark <- Visited;

                let rec visit_node acc node_list =
                        match node_list with
                        | [] -> acc
                        | n::t -> 
                                if n.n_mark = NotVisited then visit_node (dfs n acc) t 
                                else visit_node acc t
                in n.n_label::visit_node endlist n.n_link_to

        and visit_graph acc node_list = 
                match node_list with
                | [] -> acc
                | n::t -> if n.n_mark = NotVisited then visit_graph (dfs n acc) t else visit_graph acc t

        in visit_graph [] g.g_nodes

