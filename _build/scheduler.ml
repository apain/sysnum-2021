open Netlist_ast
open Graph
open Netlist_printer

exception Combinational_cycle

let read_exp eq = 
        (* read_exp : Netlist_ast.equation -> Netlist_ast.ident list *)
        let to_id a = match a with
        | Avar id -> [id]
        | _ -> []
        in
        let _, e = eq in
        match e with
        | Earg x | Enot x | Eslice (_, _, x) | Eselect (_, x) | Erom (_, _, x) | Eram (_, _, x, _, _, _) -> to_id x
        | Ereg _ -> []
        | Ebinop (_, x, y) | Econcat (x, y) -> to_id x@to_id y
        | Emux (x, y, z) -> to_id x@to_id y@to_id z
        (* | Eram (_, _, x, y, z, w) -> to_id x@to_id y@to_id z@to_id w *)

let schedule p = 
        (* schedule : Netlist_ast.program -> Netlist_ast.program *)
        let g = mk_graph () in
        let rec add_nodes l =
                match l with
                | [] -> ()
                | h::t -> let i, e = h in 
                add_node g i; 
                add_nodes t
        in
        add_nodes p.p_eqs;

        let g_labels = List.map (fun x -> x.n_label) g.g_nodes in
        let rec add_edges l = 
                match l with
                | [] -> ()
                | h::t -> let i, e = h in
                List.iter (function x -> if List.mem x g_labels then add_edge g x i) (read_exp h);
                add_edges t
        in 
        add_edges p.p_eqs;
        if has_cycle g then raise Combinational_cycle;
        let sorted_list = topological g in
        let rec sort_eqs sorted_list =
                match sorted_list with
                | [] -> []
                | h::t -> (h, List.assoc h p.p_eqs)::sort_eqs t
        in 
        {
                p_eqs = sort_eqs sorted_list;
                p_inputs = p.p_inputs;
                p_outputs = p.p_outputs;
                p_vars = p.p_vars;
        }
        

(*
 PRINT FUNCTIONS
        let ff = (Format.formatter_of_out_channel stdout) in
        print_idents ff sorted_list;
        List.iter (print_eq ff) (sort_eqs sorted_list);
        let ff = (Format.formatter_of_out_channel stdout) in 
        List.iter (function x -> print_eq ff x; Printf.printf "\n"; print_idents ff (read_exp x)) p.p_eqs;
        List.iter (print_eq ff) p.p_eqs;
        print_idents ff g_labels;

let schedule p = (
        let g = mk_graph () in
        let variables = List.map fst (Env.bindings p.p_vars) in
        List.iter (add_node g) variables;
        let rec boucle l = 
                match l with
                |[] -> ()
                |h::t -> let i, e = h in
                List.iter (function x -> add_edge g x i) (read_exp h);
                boucle t
        in 
        boucle p.p_eqs;
        try (
                let sorted_list = topological g in
                let rec sort_eqs sorted_list =
                        match sorted_list with
                        |[] -> []
                        |h::t -> (h, List.assoc h p.p_eqs)::sort_eqs t

                in {
                        p_eqs = sort_eqs sorted_list;
                        p_inputs = p.p_inputs;
                        p_outputs = p.p_outputs;
                        p_vars = p.p_vars;
        }
        ) with Cycle -> raise Combinational_cycle )
*)
(*

let schedule p = 
        (* schedule : Netlist_ast.program -> Netlist_ast.program *)
        let g = build_graph p in        
        if has_cycle g then raise Combinational_cycle;
        { p with p_eqs = topological g }

*)
(*
                let eq_map = Env.of_list p.p_eqs in
                let aux (map, next) lab = Env.add lab next map, (next+1) in
                let (var_map, nb_var) = List.fold_left aux (Env.empty, 0) sorted_list in
                let var_arr = Array.make nb_var ("", TBit) in
                let aux2 lab =
                        let i = Env.find lab var_map in
                        var_arr.(i) <- (lab, Env.find lab p.p_vars)
                in
                List.iter aux2 sorted_list;
                let rec visit l = match l with
                |[] -> []
                |var::t ->
                        if Env.mem var eq_map then begin
                                let to_neqn var_map (id, exp) = (Env.find id var_map, to_nexp var_map exp) in
                                to_neqn var_map (var, Env.find var eq_map)::visit t
                        end else
                                visit t (*variable nulle*)
                in {
                        np_eqs = visit sorted_list;
                        np_inputs = List.map (fun l -> Env.find l var_map) p.p_inputs;
                        np_outputs = List.map (fun l -> Env.find l var_map) p.p_outputs;
                        np_vars = var_arr
 
                      *) 
