open Netlist_ast

exception Invalid_input

let get_bool v = match v with
        | VBit b -> b
        | VBitArray arr -> if Array.length arr = 1 then
                arr.(0)
        else failwith "array"

let get_array v = match v with
        | VBit b -> [|b|]
        | VBitArray arr -> arr

let string_of_value v = match v with
        | VBit true -> "1"
        | VBit false -> "0"
        | VBitArray arr -> String.init (Array.length arr) (fun i -> if arr.(i) then '1' else '0')

let value_of_int s = match s with
        | 1 -> VBit true
        | 0 -> VBit false
        | _ -> raise Invalid_input 

let bool_of_char c = match c with
        | '1' -> true
        | '0' -> false
        | _ -> raise Invalid_input

let barray_of_string s = 
        Array.init (String.length s) (fun i -> bool_of_char s.[i])

let int_of_bool b = if b then 1 else 0

let int_of_arr arr =
        (* returns the value of a bit array interpreted as an integer in binary *) 
        Array.fold_left (fun n b -> (n lsl 1) + (int_of_bool b)) 0 arr 

let get_len t =
        match t with
        | TBit -> 1
        | TBitArray n -> n


